# Angular 8 GOT SEASONS CRUD


## Quick start

```bash
# clone the repo
git clone https://bitbucket.org/Jmmendezsw/got-seasons-crud.git

# change directory
cd got-seasons-crud

# install the repo with npm
npm install

# start the server
ng serve

```
in your browser go to [http://localhost:4200](http://localhost:4200) 

### Package
What you need to run this app:
* Angular version 9.0.4
* Angular CLI version 9.0.4
* Typescript version 3.7.5
* Node.js version 12.15.0 LTS (Long Term Support)
* npm (node package manager) version 6.13.7

## Getting Started


### Installation
* `npm install` (installing dependencies)

### Development
* `ng serve`
* in your browser [http://localhost:4200](http://localhost:4200) 

### Compilation
* `ng build`


### Production
* `npm build --prod`
* in your browser [http://localhost:4000](http://localhost:4000) 



### Prototype Bootstrap
* `change directory` cd ui
* launch html pages in your browser

### Author
* Updated : 03/03/2020
* Author  : jmmendezsw@gmail.com

