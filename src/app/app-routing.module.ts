import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './features/home/home.component';
import { ChapterComponent } from './features/chapters/chapters.component';
import { GotWikiComponent } from './features/got-wiki/got-wiki.component';

const routes: Routes = [
  { path: '', component: HomeComponent  },
  { path: 'chapters', component: ChapterComponent },
  { path: 'wiki', component: GotWikiComponent }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
