import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { HomeComponent } from './features/home/home.component';
import { ChapterComponent } from './features/chapters/chapters.component';
import { SeasonsService } from './core/service/seasons.service';
import { HttpClientModule } from '@angular/common/http';
import { GotWikiComponent } from './features/got-wiki/got-wiki.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    ChapterComponent,
    GotWikiComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    NgbModule,
    FormsModule
  ],
  providers: [SeasonsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
