import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";

export class Chapter {
  constructor(
    public Title: string = "",
    public Released: string = "",
    public Episode: string = "",
    public imdbRating: string = "",
    public imdbID: string = ""
  ) {}
}

export interface SeasonData {
  Title: string;
  Season: string;
  totalSeasons: string;
  Episodes: Chapter[];
  Response: string;
}

export interface WikiData {
  Title: string;
  Year: string;
  Rated: string;
  Released: string;
  Runtime: string;
  Genre: string;
  Director: string;
  Writer: string;
  Actors: string;
  Plot: string;
  Language: string;
  Country: string;
  Awards: string;
  Poster: string;
  Ratings: [
    {
      Source: string;
      Value: string;
    }
  ];
  Metascore: string;
  imdbRating: string;
  imdbVotes: string;
  imdbID: string;
  Type: string;
  totalSeasons: string;
  Response: string;
}
