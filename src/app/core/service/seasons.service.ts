import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from "@angular/common/http";

import { throwError, Observable, BehaviorSubject } from "rxjs";
import { catchError } from "rxjs/operators";
import { Chapter, WikiData } from "../models/chapter";

@Injectable({
  providedIn: "root"
})
export class SeasonsService {
  private apiUrlServer =
    "http://www.omdbapi.com/?apikey=f12ba140&t=Game%20of%20Thrones&Season=";

  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json"
    })
  };

  constructor(private httpClient: HttpClient) {}

  /**
   * Create a Chapter
   * @param chapter
   */
  createChapter(chapter: Chapter): Observable<Chapter> {
    return this.httpClient
      .post<Chapter>(
        this.apiUrlServer + "/chapters/",
        JSON.stringify(chapter),
        this.httpOptions
      )
      .pipe(catchError(this.errorHandler));
  }

  /**
   * Get Chapter by id chapter
   * @param id
   */
  getById(id: number): Observable<Chapter> {
    return this.httpClient
      .get<Chapter>(this.apiUrlServer + "/chapters/" + id)
      .pipe(catchError(this.errorHandler));
  }

  /**
   * Get the wiki info
   */
  getWiki() : Observable<WikiData> {
    return this.httpClient
    .get<WikiData>('http://www.omdbapi.com/?apikey=f12ba140&t=Game of Thrones')
    .pipe(catchError(this.errorHandler));
  }



  /**
   * Get All Chapters for Season num
   * @param num 
   */
  getAllChapters(num: number): Observable<any> {
    return this.httpClient
      .get<Chapter[]>(this.apiUrlServer + num)
      .pipe(catchError(this.errorHandler));
  }

  /**
   * Update a Chapter
   * @param id
   * @param chapter
   */
  updateChapter(id: number, chapter: Chapter): Observable<Chapter> {
    return this.httpClient
      .put<Chapter>(
        this.apiUrlServer + "/chapters/" + id,
        JSON.stringify(chapter),
        this.httpOptions
      )
      .pipe(catchError(this.errorHandler));
  }

  /**
   * Delete a Chapter
   * @param id
   */
  deleteChapter(id: number) {
    return this.httpClient
      .delete<Chapter>(this.apiUrlServer + "/chapters/" + id, this.httpOptions)
      .pipe(catchError(this.errorHandler));
  }

  errorHandler(error) {
    let errorMessage = "";
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
