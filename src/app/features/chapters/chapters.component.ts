import { SeasonsService } from "./../../core/service/seasons.service";
import { Component, OnInit, OnDestroy } from "@angular/core";
import { Chapter, SeasonData } from "../../core/models/chapter";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";

@Component({
  selector: "app-chapters",
  templateUrl: "./chapters.component.html",
  styleUrls: ["./chapters.component.scss"]
})
export class ChapterComponent implements OnInit, OnDestroy {
  chapters: Chapter[] = [];
  regModel: Chapter;
  showNew: Boolean = false;
  submitType: string = "Save";
  selectedRow: number;
  seasonSelected: number;
  destroy$: Subject<boolean> = new Subject<boolean>();
  data: SeasonData | undefined;

  constructor(private seasonsService: SeasonsService) {}

  onNew() {
    this.regModel = new Chapter();
    this.submitType = "Save";
    this.showNew = true;
  }

  onSave() {
    if (this.submitType === "Save") {
      this.chapters.push(this.regModel);
    } else {
      // Update existing

      this.chapters[this.selectedRow].Title = this.regModel.Title;
      this.chapters[this.selectedRow].Released = this.regModel.Released;
      this.chapters[this.selectedRow].Episode = this.regModel.Episode;
      this.chapters[this.selectedRow].imdbRating = this.regModel.imdbRating;
      this.chapters[this.selectedRow].imdbID = this.regModel.imdbID;
    }

    this.showNew = false;
  }

  onEdit(index: number) {
    this.selectedRow = index;
    this.regModel = new Chapter();
    // Retrieve selected
    this.regModel = Object.assign({}, this.chapters[this.selectedRow]);
    this.submitType = "Update";
    this.showNew = true;
  }

  onDelete(index: number) {
    this.chapters.splice(index, 1);
  }

  onCancel() {
    this.showNew = false;
  }

  onPlay(num: number) {
    console.log("Play the episodie " + num);
  }

  onLoadSeason(num: number) {
    this.seasonSelected = num;
    console.log("Season Loaded" + num);
    this.seasonsService
      .getAllChapters(num)
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: SeasonData) => {
        this.data = data;
        this.chapters = data.Episodes;
        console.log(this.chapters);
      });
  }

  public goodRating(rat: string) {
    if (Number(rat) > 8.8) {
      return true;
    } else {
      return false;
    }
  }

  ngOnInit() {
    // default load
    this.onLoadSeason(1);
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}
