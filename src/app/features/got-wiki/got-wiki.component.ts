import { Component, OnInit } from '@angular/core';
import { SeasonsService } from 'src/app/core/service/seasons.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { WikiData } from "../../core/models/chapter";

@Component({
  selector: 'app-got-wiki',
  templateUrl: './got-wiki.component.html',
  styleUrls: ['./got-wiki.component.scss']
})
export class GotWikiComponent implements OnInit {

  destroy$: Subject<boolean> = new Subject<boolean>();
  data: WikiData | undefined;

  active = 1;
  
  constructor(private seasonsService: SeasonsService) { }

  ngOnInit(): void {
    this.seasonsService
      .getWiki()
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: WikiData) => {
        this.data = data;
        console.log(data);
      });
  }


}
